<?php
/**
 * Copyright (C) 2011 by Nusku Networks
 * Copyright (C) 2011 by Michael Crosson <mcrosson_cloud <at> nusku <dot> net>
 *
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 * 
 *
 * Example configuration for Horde Ingo that enabled Ingo to
 * use the IMAP credentials supplied by a user instead of 
 * the Horde credentials.  This is useful when authenticating
 * against the Dovecot managesieve server.
 */

$imap_user='';
$imap_password='';

try {
	if ($GLOBALS['registry']->hasMethod('mail/imapOb')) {
		$ob = $GLOBALS['registry']->call('mail/imapOb');
		if (isset($ob)) {
			$imap_user=$ob->getParam('username');
			$imap_password=$ob->getParam('password');
		}
	}
}
catch (Exception $e) {
	Horde::logMessage('Error loading imap credentials for ingo', 'ERROR');
}

// Setup the sieve Ingo engine so it works w/ IMAP credentials found above
$backends['sieve'] = array(
    // Disabled by default
    'disabled' => false,
    'transport' => 'timsieved',
    'hordeauth' => false,
    'params' => array(
        'hostspec' => 'some.domain.com', // CHANGE ME: This should be the managesieve server
        'logintype' => 'PLAIN', // CHANGE ME: Change this if using another authentication mechanism
        'usetls' => true, // CHANGE ME: Change this if not using TLS encryption
        'port' => 4190, // CHANGE ME: This is the official IANA port for Sieve, change to 2000 as appropriate
        'scriptname' => 'horde_ingo', // CHANGE ME: Ingo will only write one script to the server, change this if you prefer a different value
        'debug' => true, // Enable this if trying to troubleshoot connections to the managesieve server
		'username' => $imap_user,
		'euser' => '', // CHANGE ME: Remove this line if not using the patch from the cloud.nusku.net general bitbucket repository
		'password' => $imap_password,
    ),
    'script' => 'sieve',
    'scriptparams' => array(),
    'shares' => false
);

// Disable IMAP filtering
// If enabled, this can cause Ingo to no longer use
// Sieve filtering
$backends['imap'] = array(
    // ENABLED by default
    'disabled' => true,
    'transport' => 'null',
    'hordeauth' => true,
    'params' => array(),
    'script' => 'imap',
    'scriptparams' => array(),
    'shares' => false
);

